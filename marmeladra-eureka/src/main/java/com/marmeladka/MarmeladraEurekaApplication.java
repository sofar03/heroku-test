package com.marmeladka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class MarmeladraEurekaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarmeladraEurekaApplication.class, args);
	}

}
