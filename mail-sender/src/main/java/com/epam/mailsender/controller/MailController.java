package com.epam.mailsender.controller;

import com.epam.mailsender.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class MailController {

    private final JavaMailSender javaMailSender;

    @Async
    @PostMapping("send")
    public void sendMail(@RequestBody Message message) {
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(message.getTo());
        msg.setSubject(message.getSubject());
        msg.setText(message.getBody());

        javaMailSender.send(msg);
    }

}
