package com.marmeladka.heroku2;

import java.util.Objects;

public class ExModel extends Model {

    private String special;

    public ExModel(String special) {
        this.special = special;
    }

    public ExModel(String var1, String var2, String special) {
        super(var1, var2);
        this.special = special;
    }

    public String getSpecial() {
        return special;
    }

    public void setSpecial(String special) {
        this.special = special;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ExModel exModel = (ExModel) o;
        return Objects.equals(special, exModel.special);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), special);
    }

}
