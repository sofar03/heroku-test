package com.marmeladka.heroku2;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "app2")
public class Controller {

    @GetMapping
    public String undefined() {
        return "Please complete the request to switch to other page";
    }

    @GetMapping("/hello")
    public String hello1(@RequestParam(name = "name", defaultValue = "World") String name) {
        return "Message to  " + name + " from module 2 !";
    }

    @GetMapping("/model")
    public Model getModel() {
        return new Model("app2", "Hello world!!!");
    }

    @GetMapping("/modelEx")
    public Object getModelEx() {
        return new ExModel("app22", "world!!!", "aaaa");
    }

}
