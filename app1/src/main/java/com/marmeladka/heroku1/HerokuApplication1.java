package com.marmeladka.heroku1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class HerokuApplication1 {

    public static void main(String[] args) {
        SpringApplication.run(HerokuApplication1.class, args);
    }

}
