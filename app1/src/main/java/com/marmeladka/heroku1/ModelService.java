package com.marmeladka.heroku1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ModelService {

    @Autowired
    private ModelClient modelClient;

    public Model getModel() {
        return modelClient.getModel();
    }

    public Object getModelEx() {
        return modelClient.getModelEx();
    }

}
