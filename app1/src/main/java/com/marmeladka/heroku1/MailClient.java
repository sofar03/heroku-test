package com.marmeladka.heroku1;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "mail-sender")
public interface MailClient {

    @PostMapping("send")
    void sendMail(@RequestBody Message message);

}
