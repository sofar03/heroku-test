package com.marmeladka.heroku1;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "app2")
public interface ModelClient {

    @GetMapping("app2/model")
    Model getModel();

    @GetMapping("app2/modelEx")
    Object getModelEx();

}
