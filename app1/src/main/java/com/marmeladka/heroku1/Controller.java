package com.marmeladka.heroku1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "app1")
public class Controller {

    @Autowired
    private MailClient mailClient;

    @Autowired
    private ModelService modelService;

    @GetMapping
    public String undefined() {
        return "Please complete the request to switch to other page";
    }

    @GetMapping("/hello")
    public String hello1(@RequestParam(name = "name", defaultValue = "World") String name) {
        return "Message to " + name + " from module 1 !";
    }

    @GetMapping("/communicate")
    public Model getModel() {
        return modelService.getModel();
    }

    @GetMapping("/communicate2")
    public Object getModelEx() {
        return modelService.getModelEx();
    }

    @PostMapping("/send")
    public void send() {
        mailClient.sendMail(new Message("projmejtej@gmail.com", "Test Feign client", "Hello from Feign"));
    }

}
